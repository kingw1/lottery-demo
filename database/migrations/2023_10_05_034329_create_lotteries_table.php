<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lotteries', function (Blueprint $table) {
            $table->id();
            $table->date('drawing_date')->nullable();
            $table->string('agent_no')->nullable();
            $table->string('no')->nullable();
            $table->string('file_url')->nullable();
            $table->string('api_public_id')->nullable();
            $table->string('api_provider')->nullable();
            $table->decimal('api_cost', 10, 3)->nullable();
            $table->integer('total_number')->nullable();
            $table->decimal('total_price', 10, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lotteries');
    }
};
