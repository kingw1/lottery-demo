<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottery Api Demo</title>

    <script src="https://cdn.tailwindcss.com"></script>

    <style>
        table {
            margin: 0 auto;
            width: 90%;
        }
        table th,td {
            border: 1px solid black;
            padding: 10px;
            text-align: center;
        }
        table input {
            border: 1px solid #ddd;
            border-radius: 5px;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div class="text-right mr-10 my-10">
        <a href="/lottery/create" class="border rounded-lg py-2 px-10 hover:bg-blue-700 hover:text-white">Create</a>
    </div>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Agent</th>
                <th>No.</th>
                <th>Total Number</th>
                <th>Total Price</th>
                <th>Api Provider</th>
                <th>Api Cost</th>
                <th>Created At</th>
                <th>View/Edit</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($lotteries as $lottery)
            <tr>
                <td>{{ $lottery->id }}</td>
                <td>{{ $lottery->drawing_date }}</td>
                <td>{{ $lottery->agent_no }}</td>
                <td>{{ $lottery->no }}</td>
                <td>{{ $lottery->total_number }}</td>
                <td>{{ number_format($lottery->total_price,2) }}</td>
                <td>{{ $lottery->api_provider }}</td>
                <td>{{ $lottery->api_cost }}</td>
                <td>{{ $lottery->created_at }}</td>
                <td><a href="/lottery/{{ $lottery->id }}/edit" class="border rounded-lg py-2 px-10 hover:bg-cyan-300 hover:text-white">View/Edit</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>