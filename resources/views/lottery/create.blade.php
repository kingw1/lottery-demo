<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottery Api Demo</title>

    <script src="https://cdn.tailwindcss.com"></script>

    <style>
        table {
            width: 90%;
        }
        table th,td {
            border: 1px solid black;
            padding: 10px;
            text-align: center;
        }
        table input {
            border: 1px solid #ddd;
            border-radius: 5px;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div class="p-20">
        <h1>upload lottery image (only jpg)</h1>
        <form method="POST" action="{{ route('lottery.store') }}" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file" accept="image/jpeg" required />
            <div class="mt-4">
                <button type="submit" class="border rounded-lg py-2 px-10 hover:bg-green-700 hover:text-white">Upload</button>
            </div>
        </form>
    </div>
</body>
</html>