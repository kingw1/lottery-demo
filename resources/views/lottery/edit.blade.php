<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottery Api Demo</title>

    <script src="https://cdn.tailwindcss.com"></script>

    <style>
        table {
            width: 90%;
        }
        table th,td {
            border: 1px solid black;
            padding: 10px;
            text-align: center;
        }
        table input {
            border: 1px solid #ddd;
            border-radius: 5px;
            padding: 5px;
        }
    </style>
</head>
<body>
    <div class="grid grid-cols-2">
        <div>
            <div class="w-2/3">
                <img src="{{ $lottery->file_url }}" class="h-full">
            </div>
        </div>
        <div>
            <form method="POST" action="{{ route('lottery.update', $lottery) }}">
                @method('patch')
                @csrf
                <table>
                    <thead>
                        <tr>
                            <th colspan="2">เลขแทง</th>
                            <th colspan="2">ราคา</th>
                        </tr>
                        <tr>
                            <th>เลข</th>
                            <th>%</th>
                            <th>ราคา</th>
                            <th>%</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($lottery->detail as $row)
                        <tr>
                            <td><input type="text" name="lottery[{{ $row->id }}][number]" value="{{ $row->number }}" class="w-full"></td>
                            <td><span class="text-{{ $row->number_confidence > 90 ? 'green' : 'red' }}-500">{{ number_format($row->number_confidence,2) }}</span></td>
                            <td><input type="text" name="lottery[{{ $row->id }}][price]" value="{{ $row->price }}" class="w-full"></td>
                            <td><span class="text-{{ $row->number_confidence > 90 ? 'green' : 'red' }}-500">{{ number_format($row->price_confidence,2) }}</span></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2" class="text-left">{{ $lottery->total_number }}</th>
                            <th colspan="2" class="text-left">{{ number_format($lottery->total_price,2) }}</th>
                        </tr>
                    </tfoot>
                </table>

                <div class="mt-4">
                    <button type="submit" class="border rounded-lg py-2 px-10 bg-green-300 hover:bg-green-700 hover:text-white">บันทึก</button>
                    <a href="/lottery" class="border rounded-lg py-3 px-10 bg-red-300 hover:bg-red-700 hover:text-white">ยกเลิก</a>
                </div>
            </form>
        </div>
    </div>
</body>
</html>