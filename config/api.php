<?php

return [
    'base_url' => env('EDENAI_URL'),
    'key' => env('EDENAI_KEY'),
    'providers' => ['amazon'],
];
