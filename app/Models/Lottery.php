<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lottery extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function detail()
    {
        return $this->hasMany(LotteryDetail::class, 'lottery_id');
    }

    public function sumTotalPrice()
    {
        $sum = 0;

        foreach ($this->detail as $detail) {
            $sum += floatval($detail->price);
        }

        return $sum;
    }
}
