<?php

namespace App\Http\Controllers;

use App\Models\Lottery;
use App\Models\LotteryDetail;
use App\Services\EdenAiService;
use Illuminate\Http\Request;

class LotteryDemoController extends Controller
{
    private $service;

    public function __construct(EdenAiService $edenAiService)
    {
        $this->service = $edenAiService;
    }

    public function index()
    {
        $lotteries = Lottery::orderBy('created_at', 'desc')->get();
        return view('lottery.index', compact('lotteries'));
    }

    public function create()
    {
        return view('lottery.create');
    }

    public function store(Request $request)
    {
        $result = $this->service->upload($request->file('file'));
        $lottery = $this->service->store($result);

        return redirect()->route('lottery.edit', $lottery);
    }

    public function edit(Request $request, Lottery $lottery)
    {
        return view('lottery.edit', compact('lottery'));
    }

    public function update(Request $request, Lottery $lottery)
    {
        foreach ($request->lottery as $lotteryDetailId => $lotteryDetailData) {
            $lotteryDetail = LotteryDetail::find($lotteryDetailId);
            $lotteryDetail->update($lotteryDetailData);
        }

        $lottery->total_number = $lottery->detail->count();
        $lottery->total_price = $lottery->sumTotalPrice();
        $lottery->save();

        return redirect()->route('lottery.index');
    }
}
