<?php

namespace App\Services;

use App\Models\Lottery;
use App\Models\LotteryDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class EdenAiService
{
    protected $providers;
    protected $baseUrl;
    protected $token;
    protected $provider;
    protected $apiLang;

    public function __construct()
    {
        $this->baseUrl = config('api.base_url');
        $this->token = config('api.key');
        $this->provider = 'amazon';
        $this->apiLang = 'en';
    }

    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    public function upload($file)
    {
        $uploadPath = $file->store('lottery', 'public');
        $fileUrl = asset('storage/' . $uploadPath);

        // $publicId = $this->getPublicIdWithUrl($fileUrl);
        $publicId = $this->getPublicIdWithFile($file);

        return [
            'file_url' => $fileUrl,
            'public_id' => $publicId,
        ];
    }

    public function result($publicId)
    {
        return Http::acceptJson()
            ->withToken($this->token)
            ->get($this->baseUrl . '/' . $publicId)
            ->json();
    }

    public function getResultData($publicId)
    {
        $result = $this->result($publicId);

        Log::debug('get api result', $result);

        if ($result['status'] == 'finished') {
            return $result;
        }

        sleep(5);

        return $this->getResultData($publicId);
    }

    /**
     * get result by public_id and store data to db
     *
     * @param array $data ['file_url', 'public_id]
     * @return Lottery $lottery
     */
    public function store(array $data)
    {
        $result = $this->getResultData($data['public_id']);

        // if error
        if (isset($result['detail'])) {
            dd($result['detail']);
        }

        if (!isset($result['results'][$this->provider])) {
            dd('no provider [' . $this->provider . '] data found');
        }

        $providerData = $result['results'][$this->provider];
        Log::debug('store api data', $providerData);

        return DB::transaction(function () use ($data, $providerData) {
            $drawingDate = date('Y-m-d');
            $agentNo = 'agent no';
            $docNo = 'no';
            $lotteryDetailData = [];
            for ($pageNo = 0; $pageNo < $providerData['num_pages']; $pageNo++) {
                $tables = $providerData['pages'][$pageNo]['tables'];
                foreach ($tables as $tableData) {
                    foreach ($tableData['rows'] as $tableRowIndex => $tableRow) {
                        if ($tableRowIndex == 0) { // except header
                            continue;
                        }

                        $numberData = $tableRow['cells'][0];
                        $priceData = $tableRow['cells'][1];

                        $lotteryDetailData[] = [
                            'number' => $numberData['text'],
                            'number_confidence' => $numberData['confidence'],
                            'price' => $priceData['text'],
                            'price_confidence' => $priceData['confidence'],
                        ];
                    }
                }
            }

            $lottery = new Lottery;
            $lottery->drawing_date = $drawingDate;
            $lottery->agent_no = $agentNo;
            $lottery->no = $docNo;
            $lottery->file_url = $data['file_url'];
            $lottery->api_public_id = $data['public_id'];
            $lottery->api_provider = $this->provider;
            $lottery->api_cost = $providerData['cost'];
            $lottery->save();

            foreach ($lotteryDetailData as $lotteryDetailRow) {
                $lotteryDetail = new LotteryDetail;
                $lotteryDetail->lottery_id = $lottery->id;
                $lotteryDetail->number = str_replace(' ', '', $lotteryDetailRow['number']);
                $lotteryDetail->number_confidence = $lotteryDetailRow['number_confidence'];
                $lotteryDetail->price = str_replace(' ', '', $lotteryDetailRow['price']);
                $lotteryDetail->price_confidence = $lotteryDetailRow['price_confidence'];
                $lotteryDetail->save();
            }

            $lottery->total_number = $lottery->detail->count();
            $lottery->total_price = $lottery->sumTotalPrice();
            $lottery->save();

            return $lottery;
        });
    }

    // upload file to api and get public_id
    public function getPublicIdWithFile($file)
    {
        $result = Http::acceptJson()
            ->withToken($this->token)
            ->attach('file', file_get_contents($file), date('YmdHis') . '.jpg')
            ->post($this->baseUrl, [
                'providers' => $this->provider,
                'language' => $this->apiLang,
            ])
            ->json();

        return $result['public_id'];
    }

    public function getPublicIdWithUrl($fileUrl)
    {
        $result = Http::acceptJson()
            ->withToken($this->token)
            ->post($this->baseUrl, [
                'providers' => $this->provider,
                'language' => $this->apiLang,
                'file_url' => $fileUrl,
            ])
            ->json();

        return $result['public_id'];
    }
}
