<?php

use App\Services\EdenAiService;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::resource('lottery', \App\Http\Controllers\LotteryDemoController::class);

Route::get('lottery-public-id', function () {
    // test 697fabcf-7a5a-44b8-b78b-3766b1336dd4
    $result = (new EdenAiService)->result('d87741bf-c357-4dd2-9b6e-e7b775eeff9c');

    dd($result);
});
